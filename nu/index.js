const nodes  = {
  fragment: function() {
    return {
      //nodeType: 11,
      childNodes: [],
     // parentNode: null,
      //nextSibling: null,
     // previousSibling: null,
      nodeName: 'fragment'
    };
  },
  comment: function(value, parent) {
    return {
     // nodeType: 8,
      nodeValue: value,
      //nextSibling: null,
      nodeName: 'comment',
     // previousSibling: null,
      //parentNode: parent || null
    };
  },
  element: function(tag, attrs, parent) {
    return {
      //nodeType: 1,
      childNodes: [],
      //nextSibling: null,
      //previousSibling: null,
      attributes: attrs || {},
      //parentNode: parent || null,
      nodeName: tag.toUpperCase()
    };
  },
  text: function(value, parent) {
    return {
      //nodeType: 3,
      childNodes: [],
      nodeValue: value,
      nodeName: 'text',
     // nextSibling: null,
     // previousSibling: null,
      //parentNode: parent || null
    };
  }
};

/**
 * HTML5 Empty Elements
 */

var empty = { area: true, base: true, basefont: true, br: true, col: true, command: true, embed: true, frame: true, hr: true, img: true, input: true, isindex: true, keygen: true, link: true, meta: true, param: true, source: true, track: true, wbr: true };

/**
 * Special tags that can contain anything
 */

var special = { script: true, style: true };

/**
 * Attributes that autofill
 */

var autofill = { autofocus: true, autoplay: true, async: true, checked: true, controls: true, defer: true, disabled: true, hidden: true, loop: true, multiple: true, open: true, readonly: true, required: true, scoped: true, selected: true }

/**
 * Regexs
 */

const rcomment = /^<!--([\s\S]*)-->/;
const rstarttag = /^<([-A-Za-z0-9_]+)((?:\s+\w+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/;
const rattr = /([-A-Za-z0-9_]+)(?:\s*=\s*(?:(?:"((?:\\.|[^"])*)")|(?:'((?:\\.|[^'])*)')|([^>\s]+)))?/g;
const rendtag = /^<\/([-A-Za-z0-9_]+)[^>]*>/;
const rtext = /^[^<]+/;

/**
 * Create regex for special tags
 */

const rspecial = {};
for (var tag in special) rspecial[tag] = new RegExp('<\/' + tag + '[^>]*>', 'i');



function Parser(html) {
  if (!(this instanceof Parser)) return new Parser(html);
  if ('string' != typeof html) throw new TypeError('String expected');
  this.html = this.original = html;
  this.tokens = [];
  this.parent = this.root = nodes.fragment();
  this.tree = this.parent.childNodes;
  this.err = null;
}

Parser.prototype.parse = function() {
  while (!this.err && this.advance() != 'eos');
  if (this.err) return this.err;

  let children = this.root.childNodes;

  // one element
  if (1 == children.length) {
    //children[0].parentNode = null;
    return children[0];
  }

  // several elements
  return this.root;
}


Parser.prototype.advance = function() {
  const tok = this.eos()
    || this.comment()
    || this.endtag()
    || this.starttag()
    || this.text()
    || this.error()

  this.tokens.push(tok);
  return tok;
}


Parser.prototype.skip = function(len){
  this.html = this.html.substr(Array.isArray(len)
    ? len[0].length
    : len);
};


Parser.prototype.eos = function() {
  if (!this.html.length) return 'eos';
};

Parser.prototype.comment = function() {
  var captures;
  if (captures = rcomment.exec(this.html)) {
    this.skip(captures);
    var node = nodes.comment(captures[1], this.parent);
   

    // connect it to the DOM
    this.connect(node);

    return 'comment';
  }

};


Parser.prototype.starttag = function() {
  var captures;
  if (captures = rstarttag.exec(this.html)) {
    this.skip(captures);
    var name = captures[1].toLowerCase();
    var attrs = (captures[2]) ? attributes(captures[2]) : {};
    var node = nodes.element(name, attrs, this.parent);
    

    // connect it to the DOM
    this.connect(node);

    // handle self-closing tags
    // and special tags that can
    // contain any content
    if (special[name]) {
      node = this.special(node);
    } else if(!empty[name]) {
      this.tree = node.childNodes;
      this.parent = node;
    }

    return 'start-tag';
  }

};

Parser.prototype.endtag = function() {
  var captures;
  if (captures = rendtag.exec(this.html)) {
    this.skip(captures);


    // move up a level
    //this.parent = this.parent.parentNode;
    this.tree = this.parent.childNodes;

    return 'end-tag';
  }
};

Parser.prototype.text = function() {
  var captures;
  if (captures = rtext.exec(this.html)) {
    this.skip(captures);
    var node = nodes.text(captures[0], this.parent);


    // connect it to the DOM
    this.connect(node);

    return 'text';
  }
};

Parser.prototype.special = function(node) {
  var name = node.nodeName.toLowerCase();
  var captures = rspecial[name].exec(this.html);
  if (!captures) return this.error('No ending ' + name + ' tag.');

  // extract the contents of the tag
  var text = this.html.slice(0, captures.index);

  // connect DOM
  var textnode = nodes.text(text, node);
  node.childNodes.push(textnode);

  // skip text + length of match
  this.skip(text.length + captures[0].length);

  return node;
};


Parser.prototype.connect = function(node) {
  // fetch the previous DOM node
  var prev = this.tree[this.tree.length - 1];

  // first child
  if (!this.tree.length) this.parent.firstChild = node;

  // add node to DOM tree
  this.tree.push(node);

  // last child
  this.parent.lastChild = node;

  // set previous and next siblings
 // if (prev) {
  //  prev.nextSibling = node;
 //   node.previousSibling = prev;
 // }

  return this;
}

Parser.prototype.error = function(err) {
  var original = this.original;
  var ellipsis = '\u2026';
  var caret = '\u2038';
  var i = original.length - this.html.length;

  // provide a useful error
  var at = original.slice(i - 20, i) + caret + original.slice(i, i + 20)
  at = original[i - 20] ? ellipsis + at : at
  at += original[i + 20] ? ellipsis : '';

  // add the message
  var msg = err || 'Parsing error.';
  msg += ' Near: ' + at;

  // set the error
  this.err = new Error(msg);

  return 'error';
};


function attributes(str) {
  let attrs = {};

  str.replace(rattr, function(match, name) {
    attrs[name] = arguments[2] || arguments[3] || arguments[4] || autofill[name] || '';
  });

  return attrs;
}
